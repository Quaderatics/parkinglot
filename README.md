# Parking Lot

## How to run
In the first level of this repository is the "parkinglot.jar" executable. To run the program, navigate to the directory where this JAR file is in a command prompt/terminal and run the command `java -jar parkinglot.jar`. Alternatively, on MacOS (and possibly Windows and other Linux systems), just double-click on the JAR file.

The window that opens contains a search bar at the top, where you can search for the first unoccupied parking space with size greater than or equal to the number entered; a grid of buttons that represent parking spaces, which you can click on to see more information; an info panel where you can edit the size of a parking space and set whether it is occupied; and a row of more buttons to add or delete parking spaces or levels.

For convenience, the first time that the program is run, it will generate a random number of parking levels, each with a random number of parking spaces that all have random sizes. When the window is closed, the program will save the parking lot data into a simple plain text file named "parkinglot.txt" in the same directory (for a real product, obviously it would need a more secure method). In subsequent runs, the program will read the parking lot in from this text file, or generate a new randomized parking lot if the file is removed. Note that I have not written any resiliency into the data retrieval, so it may crash if the file is improperly edited.

Note: The UI isn't very pretty, but everything works as it should. Probably.

## Documentation

### Class `ParkingSpace`

Represents a parking space in the parking lot. Each `ParkingSpace` object tracks its own size and whether it is currently occupied by a vehicle, as well as its floor number and index.

#### Instance variables

* `private int size`: the size of the parking space. Has a public getter and setter.
* `private boolean isOccupied`: denotes whether the parking space is occupied by a vehicle. Has a public getter and setter.
* `private int levelNumber`: the number of the floor that the parking space is on (i.e. the floor's index in the parking lot). Has a public getter.
* `private int indexInLevel`: the index of the parking space in its containing level. Has a public getter.

#### Constructors

* `public ParkingSpace(int size, boolean occupied, int levelNumber, int indexInLevel)`: creates a parking space with the given values.

### Class `ParkingLevel`

Represents one floor of the parking lot. Each `ParkingLevel` object maintains a list of the `ParkingSpace`s it contains.

#### Instance variables

* `private ArrayList<ParkingSpace> spaces`: a list of the spaces on this parking level. Does NOT have a public getter or setter - use `getSpaceAtIndex(int)` to retrieve individual parking spaces.

#### Constructors

* `public ParkingLevel(ArrayList<ParkingSpace> spaces)`: creates a parking level containing the given parking spaces.

#### Public methods

* `public int numSpaces()`: returns the number of parking spaces on this parking level.
* `public ParkingSpace getSpaceAtIndex(int index)`: returns the parking space at the given index in `spaces`, or throws an `IndexOutOfBoundsException` if the index is invalid.
* `public void addParkingSpace(int levelIndex, int spaceIndex, int size, boolean occupied)`: adds a new parking space with the given values at `spaceIndex`, or throws an exception if the index is invalid.
* `public void removeParkingSpace(int spaceIndex)`: removes the parking space at `spaceIndex`, or throws an exception if it is invalid.

### Class `ParkingLot`

Represents the parking lot structure. The `ParkingLot` object maintains a list of the `ParkingLevel`s it contains.

#### Instance variables

* `private ArrayList<ParkingLevel> levels`: a list of the levels in this parking lot. Does NOT have a public getter or setter - use `getLevelAtIndex(int)` to retrieve individual parking levels.

#### Constructors

* `public ParkingLot(ArrayList<ParkingLevel> levels)`: creates a parking lot containing the given parking levels.

#### Public methods

* `public int numLevels()`: returns the number of parking levels in this parking lot.
* `public ParkingLevel getLevelAtIndex(int index)`: returns the parking level at the given index in `levels`, or throws an `IndexOutOfBoundsException` if the index is invalid.
* `public ParkingSpace findFreeParkingSpace(int minSize)`: returns the first unoccupied parking space with size greater than or equal to `minSize`, or null if there are none.
* `public void addParkingSpace(int levelIndex, int spaceIndex, int size, boolean occupied)`: adds a new parking space at `spaceIndex` in the level at `levelIndex` with the values given, or throws an exception if the indices are invalid.
* `public void removeParkingSpace(int levelIndex, int spaceIndex)`: removes the parking space at `spaceIndex` in the level at `levelIndex`, or throws an exception if the indices are invalid.
* `public void addParkingLevel(int levelIndex, int numSpaces)`: adds a new parking level at `levelIndex` with the number of parking spaces indicated.
* `public void removeParkingLevel(int levelIndex)`: removes the parking level at `levelIndex`.

### Class `ParkingRunner`

Maintains a `ParkingLot` object and provides a user interface to interact with it, including a basic mechanism to store and retrieve it between sessions. This class contains the main method of the package.
