package main;

import java.util.ArrayList;

public class ParkingLevel {
	
	private ArrayList<ParkingSpace> spaces;
	
	//////// Constructor ////////
	
	public ParkingLevel(ArrayList<ParkingSpace> spaces) {
		super();
		this.spaces = spaces;
	}
	
	//////// Private methods ////////
	
	private boolean inBounds(int index) {
		if (index >= 0 && index < this.spaces.size()) {
			return true;
		}
		return false;
	}
	
	//////// Public methods ////////
	
	public int numSpaces() {
		return this.spaces.size();
	}
	
	public ParkingSpace getSpaceAtIndex(int index) {
		if (this.inBounds(index)) {
			return this.spaces.get(index);
		}
		throw new IndexOutOfBoundsException("No parking space at the given index (" + index + ").");
	}
	
	public void addParkingSpace(int levelIndex, int spaceIndex, int size, boolean occupied) {
		this.spaces.add(spaceIndex, new ParkingSpace(size, occupied, levelIndex, spaceIndex));
	}
	
	public void removeParkingSpace(int spaceIndex) {
		this.spaces.remove(spaceIndex);
	}

}
