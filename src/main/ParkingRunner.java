package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class ParkingRunner {

	public static void main(String[] args) {
		new ParkingRunner().start();
	}
	
	private ParkingLot lot;
	private JFrame frame;
	private ArrayList<ArrayList<JButton>> buttons;
	private JPanel buttonGridPanel;
	private ParkingSpace selectedSpace;
	private JButton selectedButton;
	private JLabel levelIndexLabel;
	private JLabel spaceIndexLabel;
	private JTextField sizeTextField;
	private JCheckBox occupiedCheckBox;
	
	public void start() {
		try {
			this.retrieveLot();
		}
		catch (Exception exception) {
			System.out.println("Couldn't retrieve a parking lot, so one will be created!");
//			exception.printStackTrace();
		}
		
		if (this.lot == null) {
			this.createLot();
		}
		
		this.displayLot();
	}
	
	private void retrieveLot() throws FileNotFoundException, IOException {
		BufferedReader reader;
		reader = new BufferedReader(new FileReader("parkinglot.txt"));
		
		int numLevels = Integer.parseInt(reader.readLine());
		ArrayList<ParkingLevel> levels = new ArrayList<ParkingLevel>(numLevels * 2);
		for (int levelIndex = 0; levelIndex < numLevels; levelIndex++) {
			int numSpaces = Integer.parseInt(reader.readLine());
			ArrayList<ParkingSpace> spaces = new ArrayList<ParkingSpace>(numSpaces * 2);
			for (int spaceIndex = 0; spaceIndex < numSpaces; spaceIndex++) {
				/* the text line representing a parking space is in the format "<size> <occupied>"
				 * where <size> is an integer and <occupied> is either "true" or "false"
				 */
				StringTokenizer tokenizer = new StringTokenizer(reader.readLine());
				int spaceSize = Integer.parseInt(tokenizer.nextToken());
				boolean spaceOccupied = Boolean.parseBoolean(tokenizer.nextToken());
				spaces.add(new ParkingSpace(spaceSize, spaceOccupied, levelIndex, spaceIndex));
			}
			levels.add(new ParkingLevel(spaces));
		}
		this.lot = new ParkingLot(levels);
		
		reader.close();
	}
	
	private void createLot() {
		int numLevels = (int) (Math.random() * 6) + 5; // [5, 10]
		ArrayList<ParkingLevel> levels = new ArrayList<ParkingLevel>(numLevels * 2);
		for (int levelIndex = 0; levelIndex < numLevels; levelIndex++) {
			int numSpaces = (int) (Math.random() * 6) + 5; // [5, 10]
			ArrayList<ParkingSpace> spaces = new ArrayList<ParkingSpace>(numSpaces * 2);
			for (int spaceIndex = 0; spaceIndex < numSpaces; spaceIndex++) {
				int spaceSize = (int) (Math.random() * 10) + 1; // [1, 10]
				spaces.add(new ParkingSpace(spaceSize, false, levelIndex, spaceIndex));
			}
			levels.add(new ParkingLevel(spaces));
		}
		this.lot = new ParkingLot(levels);
	}
	
	private void saveLot() {
		String output = "" + this.lot.numLevels() + "\n";
		for (int levelIndex = 0; levelIndex < this.lot.numLevels(); levelIndex++) {
			ParkingLevel level = this.lot.getLevelAtIndex(levelIndex);
			output += "" + level.numSpaces() + "\n";
			for (int spaceIndex = 0; spaceIndex < level.numSpaces(); spaceIndex++) {
				ParkingSpace space = level.getSpaceAtIndex(spaceIndex);
				output += "" + space.getSize() + " " + space.isOccupied() + "\n";
			}
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("parkinglot.txt"));
			writer.write(output);
			writer.close();
		}
		catch (Exception e) {
			System.out.println("Failed to save!");
		}
	}
	
	private void displayLot() {
		this.frame = new JFrame("Parking Lot");
		this.frame.setPreferredSize(new Dimension(800, 600));
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container pane = this.frame.getContentPane();
		
		this.addSearchPanel(pane);
		this.addButtonGridPanel(pane);
		this.addInfoPanel(pane);
		this.addAddRemovePanel(pane);
		
		this.frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				saveLot();
			}
		});
		
		this.frame.pack();
		this.frame.setVisible(true);
	}
	
	private void addSearchPanel(Container pane) {
		// the search area to look for an available parking space
		JPanel searchPanel = new JPanel(new BorderLayout());
		JLabel searchLabel = new JLabel("Vehicle size:");
		JTextField searchField = new JTextField();
		JButton searchButton = new JButton("Find space");
		
		ActionListener searchAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int vehicleSize;
				try {
					vehicleSize = Integer.parseUnsignedInt(searchField.getText());
				}
				catch (Exception exception) {
					showErrorMessage();
					return;
				}
				
				if (vehicleSize >= 0) {
					searchField.setText("");
					searchForSpace(vehicleSize);
				}
			}
		};
		searchField.addActionListener(searchAction); // pressing enter in the field also searches
		searchButton.addActionListener(searchAction);
		
		searchPanel.add(searchLabel, BorderLayout.LINE_START);
		searchPanel.add(searchField, BorderLayout.CENTER);
		searchPanel.add(searchButton, BorderLayout.LINE_END);
		
		pane.add(searchPanel, BorderLayout.PAGE_START);
	}
	
	private void searchForSpace(int minSize) {
		ParkingSpace space = lot.findFreeParkingSpace(minSize);
		if (space != null) {
			int levelIndex = lot.numLevels() - space.getLevelNumber() - 1; // inverted in array
			selectParkingSpace(space, this.buttons.get(levelIndex).get(space.getIndexInLevel()));
		}
	}
	
	private void addButtonGridPanel(Container pane) {
		// a grid of buttons to represent each parking space on each level of the parking lot
		this.buttonGridPanel = new JPanel();
		this.buttonGridPanel.setLayout(new BoxLayout(this.buttonGridPanel, BoxLayout.PAGE_AXIS));
		
		this.addButtons();
		
		JScrollPane scrollPane = new JScrollPane(this.buttonGridPanel);
		pane.add(scrollPane, BorderLayout.CENTER);
	}
	
	private void addButtons() {
		this.buttons = new ArrayList<ArrayList<JButton>>();
		for (int levelIndex = this.lot.numLevels() - 1; levelIndex >= 0; levelIndex--) {
			ParkingLevel level = this.lot.getLevelAtIndex(levelIndex);
			JPanel levelPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
			levelPanel.add(new JLabel("Level " + (levelIndex + 1) + ":"));
			ArrayList<JButton> levelButtons = new ArrayList<JButton>();
			
			for (int spaceIndex = 0; spaceIndex < level.numSpaces(); spaceIndex++) {
				ParkingSpace space = level.getSpaceAtIndex(spaceIndex);
				
				JButton button = new JButton("" + space.getSize());
				button.setOpaque(true);
				if (space.isOccupied()) {
					button.setBackground(Color.RED);
				}
				else {
					button.setBackground(Color.GREEN);
				}
				
				ActionListener selectAction = new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						selectParkingSpace(space, (JButton) e.getSource());
					}
				};
				button.addActionListener(selectAction);
				
				levelPanel.add(button);
				levelButtons.add(button);
			}
			
			this.buttonGridPanel.add(levelPanel);
			this.buttons.add(levelButtons);
		}
	}
	
	private void selectParkingSpace(ParkingSpace ps, JButton button) {
		this.selectedSpace = ps;
		this.selectedButton = button;
		this.levelIndexLabel.setText("Level: " + (ps.getLevelNumber() + 1));
		this.spaceIndexLabel.setText("Space: " + (ps.getIndexInLevel() + 1));
		this.sizeTextField.setText("" + ps.getSize());
		this.sizeTextField.setEnabled(true);
		this.occupiedCheckBox.setSelected(ps.isOccupied());
		this.occupiedCheckBox.setEnabled(true);
	}
	
	private void addInfoPanel(Container pane) {
		// an area to display info about a selected parking space
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.PAGE_AXIS));
		
		JLabel titleLabel = new JLabel("Parking space info");
		titleLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		infoPanel.add(titleLabel);
		
		this.levelIndexLabel = new JLabel("Level:");
		this.levelIndexLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		infoPanel.add(this.levelIndexLabel);
		
		this.spaceIndexLabel = new JLabel("Space:");
		this.spaceIndexLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		infoPanel.add(this.spaceIndexLabel);
		
		ActionListener sizeChangeAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JTextField field = (JTextField) e.getSource();
				int newSize;
				try {
					newSize = Integer.parseUnsignedInt(field.getText());
				}
				catch (Exception exception) {
					showErrorMessage();
					return;
				}
				
				if (newSize >= 0) {
					changeSize(newSize);
				}
			}
		};
		
		JPanel sizePanel = new JPanel(new BorderLayout());
		JLabel sizeLabel = new JLabel("Size:");
		sizePanel.add(sizeLabel, BorderLayout.LINE_START);
		this.sizeTextField = new JTextField();
		this.sizeTextField.addActionListener(sizeChangeAction);
		this.sizeTextField.setEnabled(false);
		sizePanel.add(this.sizeTextField, BorderLayout.CENTER);
		infoPanel.add(sizePanel);
		
		ActionListener occupiedChangeAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox checkBox = (JCheckBox) e.getSource();
				changeOccupied(checkBox.isSelected());
			}
		};
		
		this.occupiedCheckBox = new JCheckBox("Occupied");
		this.occupiedCheckBox.addActionListener(occupiedChangeAction);
		this.occupiedCheckBox.setEnabled(false);
		infoPanel.add(this.occupiedCheckBox);
		
		pane.add(infoPanel, BorderLayout.LINE_END);
	}
	
	private void changeSize(int newSize) {
		if (this.selectedSpace == null) return;
		this.selectedSpace.setSize(newSize);
		this.selectedButton.setText("" + newSize);
	}
	
	private void changeOccupied(boolean newOccupied) {
		if (this.selectedSpace == null) return;
		this.selectedSpace.setOccupied(newOccupied);
		if (newOccupied) {
			this.selectedButton.setBackground(Color.RED);
		}
		else {
			this.selectedButton.setBackground(Color.GREEN);
		}
	}
	
	private void addAddRemovePanel(Container pane) {
		// a panel to hold buttons to add/remove spaces and levels
		JPanel addRemovePanel = new JPanel();
		
		ActionListener addSpaceAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showSpaceInput(true);
			}
		};
		JButton addSpaceButton = new JButton("Add space");
		addSpaceButton.addActionListener(addSpaceAction);
		addRemovePanel.add(addSpaceButton);
		
		ActionListener removeSpaceAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showSpaceInput(false);
			}
		};
		JButton removeSpaceButton = new JButton("Remove space");
		removeSpaceButton.addActionListener(removeSpaceAction);
		addRemovePanel.add(removeSpaceButton);
		
		ActionListener addLevelAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showLevelInput(true);
			}
		};
		JButton addLevelButton = new JButton("Add level");
		addLevelButton.addActionListener(addLevelAction);
		addRemovePanel.add(addLevelButton);
		
		ActionListener removeLevelAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showLevelInput(false);
			}
		};
		JButton removeLevelButton = new JButton("Remove level");
		removeLevelButton.addActionListener(removeLevelAction);
		addRemovePanel.add(removeLevelButton);
		
		pane.add(addRemovePanel, BorderLayout.PAGE_END);
	}
	
	private void showSpaceInput(boolean adding) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		panel.add(new JLabel("Level index:"));
		JTextField levelField = new JTextField();
		panel.add(levelField);
		
		panel.add(new JLabel("Space index:"));
		JTextField spaceField = new JTextField();
		panel.add(spaceField);
		
		int result = JOptionPane.showConfirmDialog(this.frame, panel, 
				adding ? "Add parking space" : "Remove parking space", 
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			try {
				int levelIndex = Integer.parseUnsignedInt(levelField.getText()) - 1;
				int spaceIndex = Integer.parseUnsignedInt(spaceField.getText()) - 1;
				if (levelIndex >= 0 && spaceIndex >= 0) {
					if (adding) {
						this.lot.addParkingSpace(levelIndex, spaceIndex, 0, false);
					}
					else {
						this.lot.removeParkingSpace(levelIndex, spaceIndex);
					}
					
					// reload the button panel
					this.buttonGridPanel.removeAll();
					this.addButtons();
					this.frame.revalidate();
					this.frame.repaint();
				}
			}
			catch (Exception e) {
				this.showErrorMessage();
			}
		}
	}
	
	private void showLevelInput(boolean adding) {
		if (adding) this.showAddLevelInput();
		else this.showRemoveLevelInput();
	}
	
	private void showAddLevelInput() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		panel.add(new JLabel("Level index:"));
		JTextField levelField = new JTextField();
		panel.add(levelField);
		
		panel.add(new JLabel("Number of spaces:"));
		JTextField spaceField = new JTextField();
		panel.add(spaceField);
		
		int result = JOptionPane.showConfirmDialog(this.frame, panel, "Add parking level", 
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			try {
				int levelIndex = Integer.parseUnsignedInt(levelField.getText()) - 1;
				int numSpaces = Integer.parseUnsignedInt(spaceField.getText());
				if (levelIndex >= 0 && numSpaces >= 0) {
					this.lot.addParkingLevel(levelIndex, numSpaces);
					
					// reload the button panel
					this.buttonGridPanel.removeAll();
					this.addButtons();
					this.frame.revalidate();
					this.frame.repaint();
				}
			}
			catch (Exception e) {
				this.showErrorMessage();
			}
		}
	}
	
	private void showRemoveLevelInput() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		panel.add(new JLabel("Level index:"));
		JTextField levelField = new JTextField();
		panel.add(levelField);
		
		int result = JOptionPane.showConfirmDialog(this.frame, panel, "Remove parking level", 
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			try {
				int levelIndex = Integer.parseUnsignedInt(levelField.getText()) - 1;
				if (levelIndex >= 0) {
					this.lot.removeParkingLevel(levelIndex);
					
					// reload the button panel
					this.buttonGridPanel.removeAll();
					this.addButtons();
					this.frame.revalidate();
					this.frame.repaint();
				}
			}
			catch (Exception e) {
				this.showErrorMessage();
			}
		}
	}
	
	private void showErrorMessage() {
		JOptionPane.showMessageDialog(this.frame, "Invalid number(s)!", "Error", 
				JOptionPane.PLAIN_MESSAGE, null);
	}

}
