package main;

public class ParkingSpace {
	
	private int size;
	private boolean isOccupied;
	private int levelNumber;
	private int indexInLevel;
	
	//////// Constructor ////////
	
	public ParkingSpace(int size, boolean occupied, int levelNumber, int indexInLevel) {
		super();
		this.size = size;
		this.isOccupied = occupied;
		this.levelNumber = levelNumber;
		this.indexInLevel = indexInLevel;
	}
	
	//////// Getters and setters ////////
	
	public int getSize() { return this.size; }
	public void setSize(int size) { this.size = size; }
	
	public boolean isOccupied() { return this.isOccupied; }
	public void setOccupied(boolean occupied) { this.isOccupied = occupied; }
	
	public int getLevelNumber() { return this.levelNumber; }
	
	public int getIndexInLevel() { return this.indexInLevel; }

}
