package main;

import java.util.ArrayList;

public class ParkingLot {
	
	private ArrayList<ParkingLevel> levels;
	
	//////// Constructor ////////
	
	public ParkingLot(ArrayList<ParkingLevel> levels) {
		super();
		this.levels = levels;
	}
	
	//////// Private methods ////////
	
	private boolean inBounds(int index) {
		if (index >= 0 && index < this.levels.size()) {
			return true;
		}
		return false;
	}
	
	//////// Public methods //////// 
	
	public int numLevels() {
		return this.levels.size();
	}
	
	public ParkingLevel getLevelAtIndex(int index) {
		if (this.inBounds(index)) {
			return this.levels.get(index);
		}
		throw new IndexOutOfBoundsException("No parking level at the given index (" + index + ").");
	}
	
	public ParkingSpace findFreeParkingSpace(int minSize) {
		for (ParkingLevel level : this.levels) {
			for (int i = 0; i < level.numSpaces(); i++) {
				ParkingSpace space = level.getSpaceAtIndex(i);
				if (!space.isOccupied() && space.getSize() >= minSize) {
					return space;
				}
			}
		}
		return null;
	}
	
	public void addParkingSpace(int levelIndex, int spaceIndex, int size, boolean occupied) {
		this.levels.get(levelIndex).addParkingSpace(levelIndex, spaceIndex, size, occupied);
	}
	
	public void removeParkingSpace(int levelIndex, int spaceIndex) {
		this.levels.get(levelIndex).removeParkingSpace(spaceIndex);
	}
	
	public void addParkingLevel(int levelIndex, int numSpaces) {
		ArrayList<ParkingSpace> spaces = new ArrayList<ParkingSpace>();
		for (int i = 0; i < numSpaces; i++) {
			spaces.add(new ParkingSpace(0, false, levelIndex, i));
		}
		this.levels.add(levelIndex, new ParkingLevel(spaces));
	}
	
	public void removeParkingLevel(int levelIndex) {
		this.levels.remove(levelIndex);
	}

}
